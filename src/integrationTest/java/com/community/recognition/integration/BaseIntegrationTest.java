package com.community.recognition.integration;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.fluentlenium.adapter.junit.FluentTest;
import org.fluentlenium.configuration.FluentConfiguration;
import org.fluentlenium.core.domain.FluentList;
import org.fluentlenium.core.domain.FluentWebElement;
import org.fluentlenium.core.hook.wait.Wait;
import org.junit.After;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@FluentConfiguration(webDriver="chrome", pageLoadTimeout = 120000L )
@Wait
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "test")
@RunWith(SpringRunner.class)
public abstract class BaseIntegrationTest extends FluentTest {

    static boolean IsHeadless = true;
    static boolean LoadsImages = false;

    @LocalServerPort
    protected int port;

    private final String LOCAL_URL = "http://localhost";

    @Override
    public WebDriver newWebDriver() {
        return chromeBrowser();
    }

    @After
    public void baseTearDown(){
        //put your teardown logic here
    }

    private static WebDriver chromeBrowser() {
        ChromeOptions options = new ChromeOptions();

        options.addArguments("no-sandbox");
        if (IsHeadless) {
            options.addArguments("headless");
        }
        options.addArguments("disable-gpu");
        options.addArguments("window-size=1920,1080");
        options.addArguments("verbose");
//        options.addArguments("--disable-notifications");

        if (!LoadsImages) {
            HashMap<String, Object> prefs = new HashMap<>();
            prefs.put("profile.managed_default_content_settings.images", 2);
            options.setExperimentalOption("prefs", prefs);
        }

        DesiredCapabilities caps = DesiredCapabilities.chrome();
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.BROWSER, Level.ALL);

        caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        caps.setCapability(ChromeOptions.CAPABILITY, options);

        ChromeDriverService driverService = ChromeDriverService.createDefaultService();
        ChromeDriver driver = new ChromeDriver(driverService,options);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return driver;
    }

    public void login(String username, String password) {
        //put your login logic here
        goToPage("Login");
        $("input[id='username']").first().fill().withText(username);
        $("input[id='password']").first().fill().withText(password);
        $(".ui.primary.button.form-control").first().click();
    }

    public void clickOn(String selector) {
        await().pollingEvery(2000, TimeUnit.MILLISECONDS).until($(selector)).clickable();
        FluentList<FluentWebElement> element = $(selector);
        element.get(0).click();
    }

    public void clickOn(FluentWebElement element) {
        await().pollingEvery(2000, TimeUnit.MILLISECONDS).until(element).clickable();
        element.click();
    }

    public void moveMouseTo(String selectorString) {
        Actions action = new Actions(getDriver());
        By selector = By.cssSelector(selectorString);
        action.moveToElement(getDriver().findElement(selector)).build().perform();
    }

    private void navigateToLogin(){
        goTo(LOCAL_URL + port);
    }

    public void goToPage(String page){
        switch(page){
            case "Home":
                goTo(LOCAL_URL + ":"+ port +"/home");
                break;
            case "Request":
                goTo(LOCAL_URL + ":"+ port +"/request");
                break;
            case "Achievement":
                goTo(LOCAL_URL + ":"+ port +"/achievement");
                break;
            case "Approval":
                goTo(LOCAL_URL + ":"+ port +"/pendingApproval");
                break;
            case "Login":
                goTo(LOCAL_URL + ":" + port + "/login");
                break;
            default:
                goTo(LOCAL_URL + ":"+ port );
        }
    }

    public void waitUntil(String selector) {
        (new WebDriverWait(getDriver(), 5))
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(selector)));
    }
}
