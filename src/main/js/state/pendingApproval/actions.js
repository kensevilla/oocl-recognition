import axios from 'axios'
import * as types from '../../actionTypes'
import UrlUtil from "../../util/UrlUtil"
import * as statics from "../../util/statics"
import {deriveDateToday} from "../../util/DateUtil"

const url = new UrlUtil('/api/awards')

export const fetchAllPendingApproval = () => {
    return async (dispatch) => {
        const response = await axios.get(url.makeUrl(['getAllPendingApprovals']))
        dispatch({type: types.FETCH_PENDING_APPROVAL, payload: response.data})
    }
}

export const fetchApprovedAward = (id, approver) => {
    return async (dispatch) => {
        dispatch({type: types.FETCH_APPROVED_AWARD_REQUEST})
        const response = await axios.get(url.makeUrl(['getApprovedAward', `${id}`, approver]))
        dispatch({type: types.FETCH_APPROVED_AWARD_SUCCESS, payload: response.data})
        //returns data to use save in local state. reducer seems not working for RequestInfoModal
        return response.data
    }
}

export const submitApproval = (award, points, approver) => {
    return async () => {
        const getAwardResponse = await axios.get(url.makeUrl(['getAward', `${award.id}`]))
        const data = getAwardResponse.data
        data.approvalStatus = statics.REQUEST_APPROVED
        data.approvalDate = deriveDateToday()
        let urlParams = ['updateAward', `${award.id}`]
        if(points) {
            data.points = points
            urlParams.push(`${points}`)
        }
        urlParams.push(approver)
        const saveAwardResponse = await axios.post(url.makeUrl(urlParams), data)
        console.log('Award was ', saveAwardResponse.data.action, ' for ', saveAwardResponse.data.name)
    }
}

export const rejectAward = (awardId, rejectionRemarks) => {
    return async () => {
        const response = await axios.get(url.makeUrl(['getAward', `${awardId}`]))
        const data = response.data
        data.approvalStatus = statics.REQUEST_REJECTED
        data.rejectionRemarks = rejectionRemarks
        const updateAwardResponse = await axios.post(url.makeUrl(['updateAward',`${awardId}`]), data)
        console.log(updateAwardResponse.action)
    }
}