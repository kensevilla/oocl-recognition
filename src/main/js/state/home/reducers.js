import * as types from '../../actionTypes'

const initialState = {
    pendingApprovals: [],
}

export default function reducer(state = initialState, action) {
    switch (action.type) {

        case types.FETCH_PENDING_APPROVAL: {
            return {
                ...state,
                pendingApprovals: action.payload
            }
        }
        default:
            return state
    }
}