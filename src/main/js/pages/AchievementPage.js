import React, {Component} from 'react'
import {connect} from "react-redux"
import {bindActionCreators} from "redux"
import {fetchAllAwards} from "../state/achievement/actions"
import Table from "../components/Table"
import {Container, Grid, GridColumn, GridRow, Header, Image, Input, Popup, Rating} from "semantic-ui-react"
import VoyageCard from "../components/VoyageCard"
import {fetchAllMilestones} from "../state/milestone/actions"

export class AchievementPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            filterInput:''
        }
    }

    componentDidMount() {
        this.props.actions.fetchAllMilestones()
    }

    getVoyageCards() {
        return this.props.milestones.voyage.map((card, i) => (
            <div key={i} style={{width:300, height:'auto', margin: '20px 10px'}}>
                <VoyageCard data={{name: card.name, team: card.team, description: card.description, imageSrc: card.imageSrc}}/>
            </div>
        ))
    }

    handleInputFieldChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        const columns = [{
            field: 'employee',
            label: 'Employee',
            renderer: (data) => (
                <Popup trigger={
                    <Header as='h4' image>
                        <Image id='table-image' src={data.imageSrc} rounded size='mini'/>
                        <Header.Content>
                            {data.name}
                            <Header.Subheader>{data.team}</Header.Subheader>
                        </Header.Content>
                    </Header>
                }
                       inverted
                       content={'Total Containers: ' + data.stars}
                       position='bottom center'>
                </Popup>
            )
        }, {
            field: 'containerAwardStar',
            label: 'Container Award Star',
            renderer: (data) => (
                <Popup trigger={
                    <Rating icon='star' rating={data.stars % 12} maxRating={12} disabled/>
                }
                       inverted
                       content={data.stars % 12 + ' out of 12 to next voyage'}
                       position='bottom center'>
                </Popup>
            )
        }]

        let cardStyle = {
            display: 'flex',
            width: '100%',
            height: 700,
            border: '1px solid #E0E0E0',
            boxSizing: 'border-box',
            borderRadius: 5,
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            overflow: 'auto',
            flexDirection: 'row'
        }

        let filteredUsers = this.props.milestones.table.filter( (user) => {
            let {name, team} = user,
                filterInput = this.state.filterInput.toLowerCase()

            if(name.toLowerCase().includes(filterInput)){
                return true
            }

            if(team.toLowerCase().includes(filterInput)){
                return true
            }

        })

        return (
                <Grid className={'achievement-main-grid'} columns='equal' style={{display:'flex', width:'100%', height:800}}>
                    <GridRow columns={2} style={{paddingBottom: 0, display:'flex', alignContent:'flex-end'}}>
                        <GridColumn style={{margin:'auto'}}>
                            <Container>
                            <Header as='h1' style={{display: 'inline', padding: '0px 25px'}}>Completed Voyages</Header>
                            </Container>
                        </GridColumn>
                        <GridColumn style={{margin:'auto'}}>
                            <Container>
                            <Header as='h1' style={{display:'inline'}}>Container Count</Header>
                            <Input icon='filter' iconPosition='left' name={'filterInput'} style={{height: 40, width: '65%', float:'right', margin:'0px 15px'}} type="text"
                                   placeholder="Filter by Name/Team..."
                                   onChange={this.handleInputFieldChange.bind(this)}/>
                            </Container>
                        </GridColumn>
                    </GridRow>
                    <GridRow className={'base-grid-row'} stretched columns={2} style={{margin:'0px 20px'}}>
                        <GridColumn stretched >
                            <Container style={cardStyle}>
                                {this.getVoyageCards()}
                            </Container>
                        </GridColumn>
                        <GridColumn>
                            <Container className={'container-stars-list'} style={cardStyle}>
                                <Table datas={filteredUsers} columns={columns} />
                            </Container>
                        </GridColumn>
                    </GridRow>
                </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {
        milestones : state.milestones
    }
}

function mapDispatchToProps(dispatch) {
    return {actions : bindActionCreators({fetchAllAwards, fetchAllMilestones}, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(AchievementPage)