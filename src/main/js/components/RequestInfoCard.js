import React, {Component} from 'react'
import {Card, CardContent, Form, Grid, GridColumn, GridRow, TextArea} from "semantic-ui-react"
import attributes from "../util/attributes"
import {formatDate} from "../util/DateUtil"

class RequestInfoCard extends Component {
    getConvertedAttributes(data) {
        if (data.attributes) {
            let arr = data.attributes.split(",").map(item => item.trim())
            let labelAttributes = arr.map((code) => {
                const i = attributes.findIndex(attribute => attribute.code === code)
                return i > -1 ? attributes[i].label : code
            })
            return labelAttributes.join(', ')
        }
    }

    formatDisplay(title, value){
        return <div><strong>{title}:</strong> {value}</div>
    }

    displayAdditionalInfo(data) {
        if(!data.approver) {
            return
        }

        return (
            <GridRow style={{padding: 5}}>
                <GridColumn style={{padding: 0}}>
                    {this.formatDisplay("Approver", data.approver)}
                </GridColumn>
                <GridColumn style={{padding: 0}}>
                    {this.formatDisplay("Points", data.containerCount)}
                </GridColumn>
            </GridRow>
        )
    }

    render() {
        let {data} = this.props
        let supervisorValue = data.supervisor? "Yes" : "No"
        return (
            <Card fluid>
                <CardContent>
                    <Grid style={{padding: 25}} columns='equal'>
                        <GridRow style={{padding: 5}}>
                            {this.formatDisplay("Awardee", data.awardeeName)}
                        </GridRow>
                        <GridRow style={{padding: 5}}>
                            <GridColumn style={{padding: 0}}>
                                {this.formatDisplay("Team", data.awardeeTeam)}
                            </GridColumn>
                            <GridColumn style={{padding: 0}}>
                                {this.formatDisplay("Cluster", data.awardeeDepartment)}
                            </GridColumn>
                        </GridRow>
                        <GridRow/>
                        <GridRow style={{padding: 5}}>
                            {this.formatDisplay("Requestor", data.requestorName)}
                        </GridRow>
                        <GridRow style={{padding: 5}}>
                            <GridColumn style={{padding: 0}}>
                                {this.formatDisplay("Team", data.requestorTeam)}
                            </GridColumn>
                            <GridColumn style={{padding: 0}}>
                                {this.formatDisplay("Cluster", data.requestorDepartment)}
                            </GridColumn>
                        </GridRow>
                        <GridRow style={{padding: 5}}>
                            {this.formatDisplay("Awardee Supervisor", data.awardeeName ? supervisorValue : "")}
                        </GridRow>
                        <GridRow/>
                        <GridRow style={{padding: 5}}>
                            <GridColumn style={{padding: 0}}>
                                {this.formatDisplay("Request Date", formatDate(data.requestDate))}
                            </GridColumn>
                        </GridRow>
                        {this.displayAdditionalInfo(data)}
                        <GridRow/>
                        <GridRow style={{padding: 5}}>
                            <GridColumn style={{padding: 0}} width={2}>
                                {this.formatDisplay("Attributes")}
                            </GridColumn>
                            <GridColumn style={{padding: 0}}>
                                <Form>
                                    <TextArea value={this.getConvertedAttributes(data)} disabled={true}/>
                                </Form>
                            </GridColumn>
                        </GridRow>
                        <GridRow style={{padding: 5}}>
                            <GridColumn style={{padding: 0}} width={2}>
                                {this.formatDisplay("Remarks")}
                            </GridColumn>
                            <GridColumn style={{padding: 0}}>
                                <Form>
                                    <TextArea value={data.remarks} disabled={true}/>
                                </Form>
                            </GridColumn>
                        </GridRow>
                    </Grid>
                </CardContent>
            </Card>
        )
    }
}

export default RequestInfoCard