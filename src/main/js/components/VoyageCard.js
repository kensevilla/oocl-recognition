import React, {Component} from 'react'
import {Card, CardContent, CardDescription, CardHeader, CardMeta, Image} from "semantic-ui-react"

class VoyageCard extends Component {
    render() {
        let {name, team, description, imageSrc} = this.props.data
        return (
            <Card>
                <Image src={imageSrc} wrapped ui={false} />
                <CardContent style={{padding:10}}>
                    <CardHeader style={{textAlign:'center', fontSize:18}}>
                        {name}
                    </CardHeader>
                </CardContent>
                <CardMeta style={{
                    fontFamily: 'Roboto',
                    fontSize: 14,
                    color:'#000000',
                    textAlign: 'center'
                }}>
                    {team}
                </CardMeta>
                <CardDescription style={{
                    textAlign:'center',
                    fontStyle:'italic',
                    fontSize:14,
                    color:'#000000',
                    marginBottom:15
                }}>
                    {description}
                </CardDescription>
            </Card>
        )
    }
}

export default VoyageCard