import React, {Component} from "react";
import {connect} from "react-redux";

import {Button, Modal} from "semantic-ui-react";
import RequestInfoCard from "../RequestInfoCard";

const modalStyle = {
    width: 680,
    height: 'auto',
    padding: 5,
    top: 'auto',
    left: 'auto'
}
export class RequestInfoModal extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Modal size={'tiny'} open={this.props.open} style={modalStyle}
                   dimmer='blurring'>
                <Modal.Content
                    style={{alignItems: 'stretch', display: 'flex', flexDirection: 'column', padding: 10}}>
                    <RequestInfoCard data={this.props.data} />
                </Modal.Content>
                <div style={{display: 'flex', justifyContent: 'center'}}>
                    <Button id='okay-btn' size='tiny' primary
                            onClick={this.props.onCloseFn}>Okay</Button>
                </div>
            </Modal>
        )
    }
}
function mapStateToProps(state) {
    return {
        approvedAward: state.pendingApproval.approvedAward
    }
}
export default connect(mapStateToProps, null)(RequestInfoModal)