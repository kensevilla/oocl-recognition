import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import 'bootstrap/dist/css/bootstrap.css'
import {Provider} from 'react-redux'
import configureStore from './store'
import {BrowserRouter as Router} from 'react-router-dom'
import axios from "axios"
import 'semantic-ui-css/semantic.min.css'
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css'
import {logError} from "./state/app/actions"
import * as statics from "./util/statics"

const store = configureStore()

axios.interceptors.request.use(function (config) {
    return config;
}, function (error) {
    return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    if(response.request.responseURL.includes('login')){
        //redirects to login page
        window.location.href = '/login'
    }

    return response;
}, function (error) {
    if(error.request.responseURL.includes('login')){
        //redirects to home page
        window.location.href = '/'
    }

    if(error.response && statics.HTTP_ERROR_CODES.includes(error.response.status)) {
      store.dispatch(logError())
    }
    return Promise.reject(error);
});

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App/>
        </Router>
    </Provider>
    , document.getElementById('recognition'))
