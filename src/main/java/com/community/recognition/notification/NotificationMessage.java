package com.community.recognition.notification;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NotificationMessage {

    private Long awardId;
    private String action;
    private String message;
    private String image;
    private String approver;

    public static NotificationMessage createMessage(Long awardId, String message, String action, String image, String approver) {
        return new NotificationMessage(awardId, action, message, image, approver);
    }
}
