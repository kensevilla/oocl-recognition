package com.community.recognition.milestone;

import com.community.recognition.user.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name = "MILESTONE")
@AllArgsConstructor
@NoArgsConstructor
public class MilestoneEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable=false)
    private Long id;

    @Column(name = "CREATED_DATE", nullable = false)
    private Date created_date;

    @Column(name = "VOYAGE", nullable = false)
    private Long voyage;

    @Column(name = "POINTS", nullable = false)
    private Long points;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private UserEntity user;

    public void addPoints(int points) {
        this.points += points;
    }

    public void increaseVoyageByOne() {
        this.voyage += 1;
    }

    public boolean isCompletedVoyage() {
        return this.points % 12 == 0 || this.voyage < this.points / 12;
    }
}
