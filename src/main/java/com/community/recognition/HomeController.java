package com.community.recognition;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping(value = {"/", "/achievement","/home","/maintenance","/pendingApproval","/request"})
    public String index() {
        //fix for refreshing the browser window-- all needs to redirect to our SPA
        return "index.html";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }
}
