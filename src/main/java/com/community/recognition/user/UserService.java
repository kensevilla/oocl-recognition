package com.community.recognition.user;

import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity currentUser = userRepository.findByUsername(username);
        UserDetails userDetails = new User(username, currentUser.getPassword(), true, true,
                true, true, AuthorityUtils.createAuthorityList(currentUser.getRole()));
        return userDetails;
    }

    public Iterable<UserEntity> getAll(){
        return  userRepository.findAll();
    }

    public Optional<UserEntity> getUserById(Long id){
        return userRepository.findById(id);
    }

    public UserEntity getUserByUsername(String approver){
        return userRepository.findByUsername(approver);
    }

    public UserEntity addUser (UserEntity userEntity){
        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
        return userRepository.save(userEntity);
    }

    public UserEntity updateUser (UserEntity userEntity, Long id) throws Exception {
        if (!userRepository.existsById(id)) {
            throw new Exception("User id: " + id + " do not exist.");
        } else {
            return userRepository.save(userEntity);
        }
    }

    public void deleteUser (Long id) throws Exception {
            if(!userRepository.existsById(id)){
                throw new Exception("User id: " + id + " do not exist.");
            }else{
                userRepository.deleteById(id);
            }
    }

    public User getUserProfile() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return null;
        }

        if (auth.getPrincipal() instanceof User) {
            User principal = (User) auth.getPrincipal();
            return principal;
        }

        return null;
    }

}
