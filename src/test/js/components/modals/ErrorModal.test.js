import {shallow} from 'enzyme'
import React from 'react'
import {Modal, Icon, Button} from 'semantic-ui-react'
import {ErrorModal} from '../../../../main/js/components/modals/ErrorModal';
import * as appActions from '../../../../main/js/state/app/actions'

describe('Error Modal test ', () => {
    let props, wrapper, wrapperInstance, logErrorSpy

    beforeEach(() => {
        window.location.reload = jest.fn()
        logErrorSpy = jest.spyOn(appActions, 'logError')
        props = {
            actions: {
                clearError: logErrorSpy
            },
            hasError: true
        }
        wrapper = shallow(<ErrorModal {...props} />)
        wrapperInstance = wrapper.instance()
    })

    it('renders the component', () => {
        expect(wrapper).toHaveComponent(Modal)
        expect(wrapper).toHaveComponent(Icon)
        
        let item = wrapper.props().children
        expect(item[0].props.children[1]).toEqual('Application error encountered! Please refresh app...')

        let buttons = wrapper.find('Button')
        expect(buttons.length).toEqual(1)
        expect(buttons.props().content).toEqual('Okay')
        expect(buttons.props().icon).toEqual('checkmark')
    })

    it('should handle okay click', () => {
        wrapperInstance.handleOkayClick()
        expect(logErrorSpy).toHaveBeenCalled()
        expect(window.location.reload).toHaveBeenCalled()
    })
})