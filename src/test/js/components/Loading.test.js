import {shallow} from 'enzyme'
import React from "react"
import Loading from "../../../main/js/components/Loading"
import {Dimmer} from "semantic-ui-react"

describe('Loading ', () => {
    it('renders the component', () => {
        let props = {
            loading : false
        }
        const wrapper = shallow(<Loading active={props.loading}/>)
        expect(wrapper).toHaveComponent(Dimmer)
    })

})