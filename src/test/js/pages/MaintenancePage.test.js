import {shallow} from 'enzyme'
import React from "react"
import {MaintenancePage} from "../../../main/js/pages/MaintenancePage"

describe('Maintenance Page ', () => {
    let props, wrapper

    beforeEach(() => {
        props = {}
        wrapper = shallow(<MaintenancePage {...props}/>)
    })

    it('renders the component', () => {
        expect(wrapper.text()).toContain('MAINTENANCE PAGE')
    })

})