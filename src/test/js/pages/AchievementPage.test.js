import {shallow} from 'enzyme'
import React from "react"
import {AchievementPage} from "../../../main/js/pages/AchievementPage"
import Table from "../../../main/js/components/Table"
import * as milestoneActions from "../../../main/js/state/milestone/actions"
import {Input} from "semantic-ui-react"
import VoyageCard from "../../../main/js/components/VoyageCard"
import {findManyComponents} from "../testHelpers";

describe('Achievement Page', () => {
    let props, wrapper, wrapperIntance, fetchAllMilestonesSpy

    beforeEach(() => {
        fetchAllMilestonesSpy = jest.spyOn(milestoneActions, 'fetchAllMilestones')
        props = {
            actions: {
                fetchAllMilestones: fetchAllMilestonesSpy
            },
            datas: [{
                name: "John Doe",
                team: "GSP Team",
                imageSrc: '/images/avatar/johnDoe.png',
                stars: 15
            }],
            columns : [{
                field: 'employee',
                label : 'Employee',
                renderer : jest.fn()
            },{
                field: 'containerAwardStar',
                label : 'Container Award Star',
                renderer : jest.fn()
            }],
            milestones : {
                voyage : [{
                    name: 'Ziegfreid Morissey Flameño',
                    team: 'ISD',
                    description: '1 Voyage',
                    imageSrc: '/images/avatar/alien.jpeg'
                }],
                table: [{
                    id: 0,
                    imageSrc: '/images/avatar/alien.jpeg',
                    name: 'Adrian Tobias',
                    team: 'ISD',
                    stars: 10
                },{
                    id: 1,
                    imageSrc: '/images/avatar/alien.jpeg',
                    name: 'Ziegfreid Morissey Flameño',
                    team: 'ISD',
                    stars: 8
                }]
            }
        }
        wrapper = shallow(<AchievementPage {...props}/>)
        wrapperIntance = wrapper.instance()
    })

    it('renders the component', () => {
        expect(wrapper).toHaveComponent(Input)
        expect(wrapper).toHaveComponent(Table)
        expect(wrapper).toHaveComponent(VoyageCard)
    })

    it('should handle input field change', () => {
        wrapperIntance.handleInputFieldChange({target: {name: 'filterInput', value: 'Jane Doe'}})
        expect(wrapperIntance.state.filterInput).toEqual('Jane Doe')
    })

    it('should componentDidMount', () => {
        wrapperIntance.componentDidMount()
        expect(fetchAllMilestonesSpy).toHaveBeenCalled()
    })

    it('filters out items by awardeeName', () => {
        let tableComponent = findManyComponents(wrapper, Table)
        let datas = tableComponent.props().datas

        expect(datas.length).toEqual(2)

        let event = {
            target: {
                name: 'filterInput',
                value: 'Zieg'
            }
        }
        let filterInput = findManyComponents(wrapper, "Input")
        filterInput.simulate("change", event);
        wrapper.update()

        datas = findManyComponents(wrapper, Table).props().datas
        expect(datas.length).toEqual(1)
        expect(datas[0]).toEqual({
            id: 1,
            imageSrc: '/images/avatar/alien.jpeg',
            name: 'Ziegfreid Morissey Flameño',
            team: 'ISD',
            stars: 8
        })
    })

})