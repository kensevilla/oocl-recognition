import reducer from "../../../../main/js/state/maintenance/reducers"
import {checkReducerFn, clone} from "../../testHelpers"
import * as types from "../../../../main/js/actionTypes"

describe('maintenance reducers', () => {
    let checkReducer

    const initialState = {
        users: []
    }

    const initialStateClone = clone(initialState)

    beforeEach(()=>{
        checkReducer = checkReducerFn.bind(null, reducer, initialStateClone, initialState)
    })

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    })

    it('handles fetch user', () => {
        const action = {type: types.FETCH_USER, payload : [{userName : 'Dello'},{userName:'Smugglazz'}, {userName: 'Curse One'}, {userName : 'Flict G'}]}
        const expectedState = clone(initialState)
        expectedState.users = [{userName : 'Dello'},{userName:'Smugglazz'}, {userName: 'Curse One'}, {userName : 'Flict G'}]
        checkReducer(action, expectedState)
    })

})