package com.community.recognition.award;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.community.recognition.utils.AwardStatus;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class AwardControllerTest {

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Mock
    private AwardService mockAwardService;

    @Before
    public void setUp() {
        AwardController awardController = new AwardController(mockAwardService);
        mockMvc = MockMvcBuilders.standaloneSetup(awardController).build();
    }

    @Test
    public void getAllAward() throws Exception {
        List<AwardEntity> awardEntities = new ArrayList<>();
        AwardEntity awardEntity = new AwardEntity(0L, 0L, 0L, true,
            "Attributes", "Remarks", "pending", "Approval date",
            "Rejection Remarks", 20, "asd", "Awardee team","Awardee dept",
            "Requestor Team", "20190802");
        awardEntities.add(awardEntity);

        when(mockAwardService.getAll()).thenReturn(awardEntities);

        mockMvc.perform(get("/api/awards/getAll")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(awardEntities)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[*]").isNotEmpty());

    }

    @Test
    public void getAward() throws  Exception{

        Optional<AwardEntity> awardEntity = Optional.of(new AwardEntity(0L, 0L, 0L, true,
            "Attributes", "Remarks", "pending", "Approval date",
            "Rejection Remarks", 20, "asd", "Awardee team","Awardee dept",
            "Requestor Team", "20190802"));

        when(mockAwardService.getAward(0L)).thenReturn(awardEntity);

        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/awards/getAward/{id}", 0)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(0));
    }

    @Test
    public void addAward () throws Exception{
       AwardEntity awardEntity = new AwardEntity(0L, 0L, 0L, true,
            "Attributes", "Remarks", "pending", "Approval date",
            "Rejection Remarks", 20, "Awardee team","Awardee dept",
            "Requestor Team","Requestor Dept", "20190802");

        when(mockAwardService.saveAward(awardEntity)).thenReturn(awardEntity);

        mockMvc.perform( MockMvcRequestBuilders
                .post("/api/awards/saveAward")
                .content(objectMapper.writeValueAsString(new AwardEntity(0L, 0L, 0L, true,
            "Attributes", "Remarks", "pending", "Approval date",
            "Rejection Remarks", 20, "Awardee team","Awardee dept",
            "Requestor Team","Requestor Dept", "20190802")))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void updateAward () throws Exception{
        AwardEntity origAwardEntity = new AwardEntity(1L, 0L, 0L, true,
            "Attributes", "Remarks", "pending", "Approval date",
            "Rejection Remarks", 20, "Awardee team","Awardee dept",
            "Requestor Team","Requestor Dept", "20190802");

        AwardResponse awardResponse = new AwardResponse(AwardStatus.APPROVED, "John Doe");

        when(mockAwardService.updateAward(origAwardEntity,1L, 2, "daluzde")).thenReturn(awardResponse);

        mockMvc.perform(MockMvcRequestBuilders
            .post("/api/awards/updateAward/{id}/{points}/{approver}", "1", 2, "daluzde")
            .content(objectMapper.writeValueAsString(origAwardEntity))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
            .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().isOk())
            .andExpect(content().json(objectMapper.writeValueAsString(awardResponse)));
    }

    @Test
    public void deleteAward() throws Exception
    {
        AwardEntity origAwardEntity = new AwardEntity(1L, 0L, 0L, true,
            "Attributes", "Remarks", "pending", "Approval date",
            "Rejection Remarks",  20, "Awardee team","Awardee dept",
            "Requestor Team","Requestor Dept", "20190802");
        mockAwardService.saveAward(origAwardEntity);
        mockAwardService.deleteAward(1L);

        mockMvc.perform( MockMvcRequestBuilders.delete("/api/awards/deleteAward/{id}", 1) )
                .andExpect(status().isAccepted());
    }

    @Test
    public void getAllPendingApprovals() throws Exception {
        PendingApprovalResponse response = new PendingApprovalResponseBuilder().initialData();
        when(mockAwardService.getAllByApprovalStatus(AwardStatus.PENDING)).thenReturn(Collections.singletonList(response));

        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/awards/getAllPendingApprovals", 0)
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(Collections.singletonList(response))));
    }

    @Test
    public void getApprovedAward() throws Exception {
        PendingApprovalResponse response = new PendingApprovalResponseBuilder().initialData();
        when(mockAwardService.getApprovedAwardById(1L, "Denniz Daluz")).thenReturn(response);

        mockMvc.perform( MockMvcRequestBuilders
                .get("/api/awards/getApprovedAward/{id}/{approver}", 1, "Denniz Daluz")
                .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(response)));
    }
}